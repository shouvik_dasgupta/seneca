<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<tags:template>
	<jsp:attribute name="head">  
		<script type="text/javascript">
			// inline JavaScript here 
		</script>
  	</jsp:attribute>  
	<jsp:body>
 		<table align="center" border="1" width = "400" height = "150">
 			<th colspan="2"><h2>Welcome to Vendor Management</h2></th>
  			<tr>
    			<td>Add Vendor</td><td><a href="addvendor">Click here</a> </td>
  			</tr>
  			<tr>
    			<td>Delete Vendor</td><td><a href="deletevendor">Click here</a> </td>
  			</tr>
  			<tr>
    			<td>Show All Vendors</td><td><a href="showallvendors">Click here</a> </td>
  			</tr>
  			<!-- <a href="vendormanage" align="center">Go to Homepage</a> -->
  		</table>
	</jsp:body>
</tags:template>