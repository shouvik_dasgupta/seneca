<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>
<tags:template>
	<jsp:attribute name="head">  
		<script type="text/javascript">
			// inline JavaScript here 
		</script>
  	</jsp:attribute>  
	<jsp:body>
 		<table align="center" border="1" width="500" height="150">
 			<tr><td colspan="2" align="center"><h2>Welcome to Vendor and Task Management</h2></td></tr>
  			<tr>
    			<td  align="center">Vendor Management</td><td  align="center"><a href="vendormanage">Click here</a> </td>
  			</tr>
  			<tr>
    			<td  align="center">Task Management</td><td  align="center"><a href="taskmanage">Click here</a> </td>
  			</tr>
  		</table>
	</jsp:body>
</tags:template>