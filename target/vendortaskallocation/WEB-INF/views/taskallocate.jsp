<%@taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<tags:template>
	<jsp:attribute name="head">  
		<script type="text/javascript">
		function isNumericVendorid() {
			var input = document.getElementById('id').value;
			var flag = false;
			flag = !isNaN(parseFloat(input)) && isFinite(input);
			if (flag != true) {
				alert("Please enter valid numeric characters for Vendor ID:");
				return false;
			} else {
				return true;
			}
		}
		function requiredVendorid() {
			var mystring = document.getElementById('id').value;
			if (!mystring.match(/\S/)) {
				alert("Empty value is not allowed for Vendor ID:");
				return false;
			} else {
				return true;
			}
		}
		function isNumericTaskid() {
			var input = document.getElementById('taskid').value;
			var flag = false;
			flag = !isNaN(parseFloat(input)) && isFinite(input);
			if (flag != true) {
				alert("Please enter valid numeric characters for Task ID:");
				return false;
			} else {
				return true;
			}
		}
		function requiredTaskid() {
			var mystring = document.getElementById('taskid').value;
			if (!mystring.match(/\S/)) {
				alert("Empty value is not allowed for Task ID:");
				return false;
			} else {
				return true;
			}
		}
		function validateForm() {
			var flag = true;
			flag = requiredVendorid();
			if (flag == false) {
				return false;
			}
			flag = isNumericVendorid();
			if (flag == false) {
				return false;
			}
			flag = requiredTaskid();
			if (flag == false) {
				return false;
			}
			flag = isNumericTaskid();
			if (flag == false) {
				return false;
			}
			return true;
		}
		</script>
  	</jsp:attribute>
	<jsp:body>
		<form method="post" action="processtaskallocate" modelAttribute="tapvo" onclick="validateForm()">
		<table align="center" border="1" width = "400">
			<tr>
				<td align="center" colspan="2"><h2>Task allocation<h2></h2></td>
			</tr>
			<tr>
				<td>Enter number of tasks</td>
				<td><input type="text" name="taskno" value="${tapvo.taskno}"/></td>	
          	</tr>
  			<tr>
    			<th>Vendor ID</th>
				<th>Task percent</th>
			</tr>
  			<c:forEach items="${tapvo.volist}" var="vo" varStatus="status">
        		<tr>
          			<td align"center"><c:out value="${vo.vendorid}" /></td>
          			<td><input type="text" name="volist[${status.index}].percent"/></td>
        		</tr>
        		<tr>
        			<td><input name="volist[${status.index}].vendorid" type="hidden" value="${vo.vendorid}"/></td>
        		</tr>
      		</c:forEach>
      		<tr>
        		<td colspan="2" align="center"><input type="submit" name="submit" value="submit"></td>
        	</tr>
  		</table>
  		<!-- <a href="vendormanage" align="center">Go to Homepage</a> -->
  		</form>
	</jsp:body>
</tags:template>