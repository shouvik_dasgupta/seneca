<%@taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<tags:template>
	<jsp:attribute name="head">  
		<script type="text/javascript">
		function isNumericTaskid() {
			var input = document.getElementById('taskid').value;
			var flag = false;
			flag = !isNaN(parseFloat(input)) && isFinite(input);
			if (flag != true) {
				alert("Please enter valid numeric characters for Task ID:");
				return false;
			} else {
				return true;
			}
		}
		function requiredTaskid() {
			var mystring = document.getElementById('taskid').value;
			if (!mystring.match(/\S/)) {
				alert("Empty value is not allowed for Task ID:");
				return false;
			} else {
				return true;
			}
		}
		function validateForm() {
			var flag = true;
			flag = requiredVendorid();
			if (flag == false) {
				return false;
			}
			flag = isNumericVendorid();
			if (flag == false) {
				return false;
			}
			return true;
		}
		</script>
  	</jsp:attribute>
	<jsp:body>
		<form name="frm" method="post" action="processdeletetask" modelAttribute="taskid" onclick="validateForm()">
		<table align="center" border="1" width="400" height="150">
		<tr>
					<td align="center" colspan="2"><h2>Delete a Task</h2></td>
				</tr>
  			<tr>
    			<td>Task ID</td>
    			<td><input type="text" name="taskid" value="${taskid}"></td>
        	</tr>
      		<tr>
        		<td colspan="2" align="center"><input type="submit" name="submit" value="submit"></td>
        	</tr>
  		</table>
  		</form>
	</jsp:body>
</tags:template>