<%@taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<tags:template>
	<jsp:attribute name="head">  
		<script type="text/javascript">
			// inline JavaScript here
		</script>
  	</jsp:attribute>
	<jsp:body>
		<table align="center" border="1" width = "400">
			<tr><td colspan="2"  align="center"><h2>List of all vendors</h2></td></tr>
  			<tr>
    			<th>Vendor ID</th>
				<th>Vendor Name</th>
			</tr>
  			<c:forEach items="${vendors}" var="vendor">
        		<tr>
          			<td><c:out value="${vendor.id}" /></td>
          			<td><c:out value="${vendor.name}" /></td>
        		</tr>
      		</c:forEach>
      		<tr>
      			<td align="center"><a href="hello">Go to Homepage</a></td>
      			<td align="center"><a href="vendormanage">Go to Vendor Management</a></td>
      		</tr>
  		</table>
	</jsp:body>
</tags:template>