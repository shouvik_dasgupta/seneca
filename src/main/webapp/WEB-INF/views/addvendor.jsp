<%@taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<tags:template>
	<jsp:attribute name="head">  
		<script type="text/javascript">
			function isNumericVendorid() {
				var input = document.getElementById('id').value;
				var flag = false;
				flag = !isNaN(parseFloat(input)) && isFinite(input);
				if (flag != true) {
					alert("Please enter valid numeric characters for Vendor ID:");
					return false;
				} else {
					return true;
				}
			}
			function requiredVendorid() {
				var mystring = document.getElementById('id').value;
				if (!mystring.match(/\S/)) {
					alert("Empty value is not allowed for Vendor ID:");
					return false;
				} else {
					return true;
				}
			}
			function validateForm() {
				var flag = true;
				flag = requiredVendorid();
				if (flag == false) {
					return false;
				}
				flag = isNumericVendorid();
				if (flag == false) {
					return false;
				}
				return true;
			}
		</script>
  	</jsp:attribute>
	<jsp:body>
		<form name="frm" method="post" action="processaddvendor" modelAttribute="vendor" onsubmit="return validateForm()">
		<table align="center" border="1" width="400" height="150">
			<tr>
					<td align="center" colspan="2"><h2>Add a Vendor</h2></td>
				</tr>
  			<tr>
    			<th>Vendor ID</th>
				<th>Vendor Name</th>
			</tr>
        		<tr>
          			<td><input type="text" name="id" id="id" value="${id}"></td>
          			<td><input type="text" name="name" id="name"
						value="${name}"></td>
        		</tr>
      		<tr>
        		<td colspan="2" align="center"><input type="submit"
						name="submit" value="submit"></td>
        	</tr>
  		</table>
  		<!-- <a href="vendormanage" align="center">Go to Homepage</a> -->
  		</form>
	</jsp:body>
</tags:template>