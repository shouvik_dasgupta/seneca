package com.seneca.vendortaskallocation.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seneca.vendortaskallocation.persistence.Task;
import com.seneca.vendortaskallocation.persistence.TaskAllocationPercentVO;
import com.seneca.vendortaskallocation.persistence.TaskAllocationVO;
import com.seneca.vendortaskallocation.persistence.TaskDAO;
import com.seneca.vendortaskallocation.persistence.Vendor;
import com.seneca.vendortaskallocation.persistence.VendorDAO;

/**
 * This class represents the Service class for Task Allocation.
 * @author Shouvik
 *
 */
@Service("allocation")
public class AllocationService {

	@Autowired
	private VendorDAO vendorDAO;

	@Autowired
	private TaskDAO taskDAO;

	/**
	 * Allocate tasks based on the input from UI.
	 * @param aTaskAllocationPercentVO
	 */
	@Transactional
	public void allocateTask(TaskAllocationPercentVO aTaskAllocationPercentVO) {
		int starttaskid = taskDAO.getMaxTaskid()+1;
		List<Task> list=new ArrayList<>();
		
		for(TaskAllocationVO a:aTaskAllocationPercentVO.getVolist()){
			convert2TaskList_1(a,aTaskAllocationPercentVO.getTaskno());
		}
		
		int sum =0;
		for(TaskAllocationVO a:aTaskAllocationPercentVO.getVolist()){
			sum=sum+a.getNbroftasks();
		}
		if(sum<aTaskAllocationPercentVO.getTaskno()){
			int n=aTaskAllocationPercentVO.getVolist().size();
			TaskAllocationVO t=aTaskAllocationPercentVO.getVolist().get(n-1);
			int v=t.getNbroftasks()+(aTaskAllocationPercentVO.getTaskno()-sum);
			t.setNbroftasks(v);
		}
		for(TaskAllocationVO a:aTaskAllocationPercentVO.getVolist()){
			starttaskid=convert2TaskList_2(a,starttaskid,list);
		}
		for(Task task:list){
			taskDAO.add(task);
		}
	}
	
	/**
	 * Convert TaskAllocationVO to start start task id.
	 * @param a
	 * @param starttaskid
	 * @param list
	 * @return
	 */
	private int convert2TaskList_2(TaskAllocationVO a, int starttaskid, List<Task> list) {
		int ctr=a.getNbroftasks();
		Task t=null;
		for(int i=0;i<ctr;i++){
			t=new Task();
			t.setTaskid(starttaskid);
			t.setVendorid(a.getVendorid().toString());
			starttaskid++;
			list.add(t);
		}
		return starttaskid;
	}

	/**
	 * Set number of tasks.
	 * @param a
	 * @param num
	 */
	private void convert2TaskList_1(TaskAllocationVO a, int num) {
		Double percent=Double.parseDouble(a.getPercent());
		double ratio=percent/100;
		int c=(int)(ratio*num);
		a.setNbroftasks(c);
	}

	/**
	 * Get list of Tasks.
	 * @param starttaskid
	 * @param aTaskAllocationPercentVO
	 * @return
	 */
	List<Task> getTasklist(int starttaskid,TaskAllocationPercentVO aTaskAllocationPercentVO){
		List<Task> list=new ArrayList<>();
		setVendorTasks(aTaskAllocationPercentVO);
		List<TaskAllocationVO> vendortasklist = aTaskAllocationPercentVO.getVolist();
		for(TaskAllocationVO aTaskAllocationVO:vendortasklist){
			int num=aTaskAllocationVO.getNbroftasks();
			Task task=null;
			for(int i=0;i<num;i++){
				task=new Task();
				task.setVendorid(aTaskAllocationVO.getVendorid().toString());
				task.setTaskid(starttaskid+i);
				starttaskid++;
				list.add(task);
			}
		}
		return list;
	}

	/**
	 * Set TaskAllocationPercentVO values
	 * @param aTaskAllocationPercentVO
	 */
	private void setVendorTasks(TaskAllocationPercentVO aTaskAllocationPercentVO){
		int num=aTaskAllocationPercentVO.getTaskno();
		TaskAllocationVO aTaskAllocationVO = null;
		Integer nbroftasks=null;
		int sum=0;
		for(int i=0;i<aTaskAllocationPercentVO.getVolist().size()-1;i++){
			aTaskAllocationVO = aTaskAllocationPercentVO.getVolist().get(i);
			float taskratio=Integer.parseInt(aTaskAllocationVO.getPercent())/100;
			nbroftasks=Math.round((taskratio*num));
			sum=sum+nbroftasks;
			aTaskAllocationVO.setNbroftasks(nbroftasks);
		}
		if(aTaskAllocationPercentVO!=null && aTaskAllocationPercentVO.getVolist()!=null && aTaskAllocationPercentVO.getVolist().size()!=0){
			aTaskAllocationVO = aTaskAllocationPercentVO.getVolist().get(aTaskAllocationPercentVO.getVolist().size()-1);
		}
		aTaskAllocationVO.setNbroftasks(num-sum);
	}

	/**
	 * Add a Vendor
	 * @param vendor
	 */
	@Transactional
	public void addVendor(Vendor vendor) {
		vendorDAO.addvendor(vendor);
	}

	/**
	 * Get all Vendors
	 * @return
	 */
	@Transactional
	public List<Vendor> getAllVendors() {
		return vendorDAO.getAllVendors();
	}

	/**
	 * Get all Tasks
	 * @return
	 */
	@Transactional
	public List<Task> getAllTasks() {
		return taskDAO.getAllTasks();
	}

	/**
	 * Add a Task
	 * @param task
	 */
	@Transactional
	public void addTask(Task task) {
		taskDAO.add(task);
	}

	/**
	 * Delete a Vendor
	 * @param vendor
	 */
	@Transactional
	public void deleteVendor(Vendor vendor) {
		vendorDAO.delete(vendor.getId());
	}

	/**
	 * Delete a Task.
	 * @param taskid
	 */
	@Transactional
	public void deleteTask(Integer taskid) {
		taskDAO.delete(taskid);
	}
}
