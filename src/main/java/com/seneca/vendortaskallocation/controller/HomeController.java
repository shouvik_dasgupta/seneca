package com.seneca.vendortaskallocation.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.seneca.vendortaskallocation.persistence.Task;
import com.seneca.vendortaskallocation.persistence.TaskAllocationPercentVO;
import com.seneca.vendortaskallocation.persistence.TaskAllocationVO;
import com.seneca.vendortaskallocation.persistence.Vendor;
import com.seneca.vendortaskallocation.service.AllocationService;

/**
 * This class is the Spring MVC Controller class.
 * @author Shouvik
 *
 */
@Controller
public class HomeController {
	
	@Autowired
	private AllocationService service;

	@RequestMapping(value="/hello")
	public ModelAndView hello() {
		ModelAndView ret = new ModelAndView("home");
		return ret;
	}
	
	@RequestMapping(value="/vendormanage")
	public ModelAndView vendormanage() {
		ModelAndView ret = new ModelAndView("vendormanage");
		return ret;
	}
	
	@RequestMapping(value="/addvendor")
	public String addvendor(Model model) {
		Vendor vendor=new Vendor();
		model.addAttribute("vendor",vendor);
		return "addvendor";
	}
	
	@RequestMapping(value="/processaddvendor")
	public String processaddvendor(@ModelAttribute("vendor") Vendor vendor) {
		service.addVendor(vendor);
		return "redirect:/showallvendors";
	}

	@RequestMapping(value="/deletevendor")
	public String deletevendor(@ModelAttribute("vendor") Vendor vendor) {
		return "deletevendor";
	}
	
	@RequestMapping(value="/processdeletevendor")
	public String processdeletevendor(@ModelAttribute("vendor") Vendor vendor) {
		service.deleteVendor(vendor);
		return "redirect:/showallvendors";
	}
	
	@RequestMapping(value="/showallvendors")
	public ModelAndView showallvendors() {
		ModelAndView ret = new ModelAndView("showallvendors");
		List<Vendor> vendors= service.getAllVendors();
		ret.addObject("vendors", vendors);
		return ret;
	}
	
	@RequestMapping(value="/taskmanage")
	public ModelAndView taskmanage() {
		ModelAndView ret = new ModelAndView("taskmanage");
		return ret;
	}
	
	@RequestMapping(value="/showalltasks")
	public ModelAndView showalltasks() {
		ModelAndView ret = new ModelAndView("showalltasks");
		List<Task> tasks= service.getAllTasks();
		ret.addObject("tasks", tasks);
		return ret;
	}
	
	@RequestMapping(value="/processtaskallocate", method = RequestMethod.POST)
	public String processtaskallocate(@ModelAttribute("tapvo") TaskAllocationPercentVO tapvo) {
		service.allocateTask(tapvo);
		return "redirect:/showalltasks";
	}
	
	@RequestMapping(value="/taskallocate")
	public String taskallocate(Model model) {
		TaskAllocationPercentVO tapvo=new TaskAllocationPercentVO();
		
		List<Vendor> vendorList = service.getAllVendors();
		List<TaskAllocationVO> list=new ArrayList<TaskAllocationVO>();
		for(int i=0;i<vendorList.size();i++){
			TaskAllocationVO aTaskAllocationVO=null;
			aTaskAllocationVO=new TaskAllocationVO();
			aTaskAllocationVO.setVendorid(vendorList.get(i).getId());
			list.add(aTaskAllocationVO);
		}
		tapvo.setVolist(list);
		model.addAttribute("tapvo", tapvo);
		return "taskallocate";
	}

	@RequestMapping(value="/addtask")
	public String addtask(Model model) {
		Task task=new Task();
		model.addAttribute("task",task);
		return "addtask";
	}

	@RequestMapping(value="/deletetask")
	public String deletetask(@ModelAttribute("taskid") String taskid) {
		return "deletetask";
	}
	
	@RequestMapping(value="/processaddtask")
	public String processaddtask(@ModelAttribute("task") Task task) {
		service.addTask(task);
		return "redirect:/showalltasks";
	}

	@RequestMapping(value="/processdeletetask")
	public String processdeletetask(@ModelAttribute("taskid") String taskid) {
		service.deleteTask(Integer.parseInt(taskid));
		return "redirect:/showalltasks";
	}
	
}
