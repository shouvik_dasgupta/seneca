package com.seneca.vendortaskallocation.persistence;

public class TaskAllocationVO {
	
	private Integer vendorid;
	
	private String percent;
	
	private Integer nbroftasks;

	public Integer getNbroftasks() {
		return nbroftasks;
	}

	public void setNbroftasks(Integer nbroftasks) {
		this.nbroftasks = nbroftasks;
	}

	public Integer getVendorid() {
		return vendorid;
	}

	public void setVendorid(Integer vendorid) {
		this.vendorid = vendorid;
	}

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	@Override
	public String toString() {
		return "TaskAllocationVO [vendorid=" + vendorid + ", percent=" + percent + ", nbroftasks=" + nbroftasks + "]";
	}

}
