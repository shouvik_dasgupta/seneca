package com.seneca.vendortaskallocation.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class is the corresponding JPA Entity class for the table VENDOR in APP schema.
 * @author Shouvik
 */
@Entity
@Table(name = "APP.VENDOR")
public class Vendor{
	
	@Column(name = "VENDORNAME")
	private String name;
	
	@Id
    @Column(name = "VENDORID")
	private Integer id;

	public String getName() {
		return name;
	}

	public void setName(String vendorname) {
		this.name = vendorname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Vendor [vendorname=" + name + ", vendorid=" + id + "]";
	}

}
