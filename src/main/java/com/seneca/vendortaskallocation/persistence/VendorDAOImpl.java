package com.seneca.vendortaskallocation.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

/**
 * This class is the DAO interface implementation for VENDOR table
 * @author Shouvik
 */
@Component
public class VendorDAOImpl implements VendorDAO{

	@PersistenceContext
	private EntityManager em;

	/**
	 * Add a Vendor.
	 */
	@Override
	@Transactional
	public void addvendor(Vendor vendor) {
		Vendor avendor=em.find(Vendor.class,vendor.getId());
		if(avendor==null){
			em.persist(vendor);
		}
	}
	
	/**
	 * Get all vendors.
	 */
	@Override
	@Transactional
    public List<Vendor> getAllVendors() {
		TypedQuery<Vendor> query = em.createQuery("SELECT v FROM Vendor v order by v.id", Vendor.class);
        List<Vendor> result = query.getResultList();
        return result;
    }

	/**
	 * Delete a Vendor.
	 */
	@Override
	@Transactional
	public void delete(Integer vendorid) {
		Vendor vendor=getVendor(vendorid);
		if(vendor!=null){
			em.remove(vendor);
		}	
	}
	
	/**
	 * Get a Vendor corresponding to Vendor ID.
	 */
	@Override
	@Transactional
    public Vendor getVendor(Integer vendorid) {
		Vendor vendor=em.find(Vendor.class,vendorid);
        return vendor;
    }
	
	/**
	 * Get the Maximum Vendor ID.
	 */
	@Override
	@Transactional
    public Integer getMaxVendorid(){
		Integer i=(Integer)em.createQuery("select max(v.id) from Vendor v").getSingleResult();
		return i;
	}

}
