package com.seneca.vendortaskallocation.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class is the corresponding JPA Entity class for the table TASK in APP schema.
 * @author Shouvik
 *
 */
@Entity
@Table(name = "APP.TASK")
public class Task{

	@Id
    @Column(name = "TASKID")
    private Integer taskid;
 
    @Column(name = "VENDORID")
    private String vendorid;

	public Integer getTaskid() {
		return taskid;
	}

	public void setTaskid(Integer taskid) {
		this.taskid = taskid;
	}

	public String getVendorid() {
		return vendorid;
	}

	public void setVendorid(String vendorid) {
		this.vendorid = vendorid;
	}

	@Override
	public String toString() {
		return "Task [taskid=" + taskid + ", vendorid=" + vendorid + "]";
	}
	
}
