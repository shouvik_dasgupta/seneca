package com.seneca.vendortaskallocation.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class is the DAO interface implementation for TASK table
 * @author Shouvik
 */
@Component
public class TaskDAOImpl implements TaskDAO {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Add a Task.
	 */
	@Override
	@Transactional
	public void add(Task task) {
		Task atask=em.find(Task.class,task.getTaskid());
		if(atask==null){
			em.persist(task);
		}
	}
	
	/**
	 * Delete a Task.
	 */
	@Override
	@Transactional
	public void delete(Integer taskid){
		Task atask=em.find(Task.class,taskid);
		if(atask!=null){
			em.remove(atask);
		}
	}
	
	/**
	 * Get task corresponding to a Task ID.
	 */
	@Override
	@Transactional
	public Task getTask(Integer taskid){
		Task task=em.find(Task.class,taskid);
		return task;
	}
	
	/**
	 * Get all Tasks in the Task table.
	 */
	@Override
	@Transactional
	public List<Task> getAllTasks(){
		TypedQuery<Task> query = em.createQuery("SELECT t FROM Task t order by t.taskid", Task.class);
        List<Task> result = query.getResultList();
        return result;
	}
	
	/**
	 * Get the maximum Task ID in the Task table.
	 */
	@Override
	@Transactional
    public Integer getMaxTaskid(){
		Integer i=(Integer)em.createQuery("select max(t.taskid) from Task t").getSingleResult();
		return i;
	}

}
