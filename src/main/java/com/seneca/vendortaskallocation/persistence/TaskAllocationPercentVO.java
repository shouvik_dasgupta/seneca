package com.seneca.vendortaskallocation.persistence;

import java.util.List;

/**
 * This class is the Value Object representing Task Allocation Percent data
 * @author Shouvik
 */
public class TaskAllocationPercentVO {
	
	private Integer taskno;
	
	private List<TaskAllocationVO> volist;

	public Integer getTaskno() {
		return taskno;
	}

	public void setTaskno(Integer taskno) {
		this.taskno = taskno;
	}

	public List<TaskAllocationVO> getVolist() {
		return volist;
	}

	public void setVolist(List<TaskAllocationVO> volist) {
		this.volist = volist;
	}

	@Override
	public String toString() {
		return "TaskAllocationPercentVO [taskno=" + taskno + ", volist=" + volist + "]";
	}

}
