package com.seneca.vendortaskallocation.persistence;

import java.util.List;

/**
 * This is the DAO interface for TASK table
 * @author Shouvik
 */
public interface VendorDAO {
	
	public void addvendor(Vendor vendor);
	
	public void delete(Integer vendorid) ;

    public List<Vendor> getAllVendors();
    
    public Vendor getVendor(Integer id);

	Integer getMaxVendorid();
    
}
