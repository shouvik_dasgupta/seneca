package com.seneca.vendortaskallocation.persistence;

import java.util.List;

/**
 * This is the DAO interface for TASK table
 * @author Shouvik
 */
public interface TaskDAO {

	public void add(Task task) ;

	void delete(Integer taskid);

	Task getTask(Integer taskid);

	List<Task> getAllTasks();

	Integer getMaxTaskid();
}

